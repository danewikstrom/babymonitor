//
//  MonitorViewController.swift
//  BabyMonitor
//
//  Created by Dane Wikstrom on 11/12/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//


import UIKit
import Foundation
import RealmSwift
import Parse

class MonitorViewController: UIViewController, BambuserPlayerDelegate {
    var bambuserPlayer: BambuserPlayer
    var applicationID: String
    var resourceUri: String
    var currentUser = PFUser.current()
    let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(MonitorViewController.back(sender:)))
    
    required init?(coder aDecoder: NSCoder) {
        bambuserPlayer = BambuserPlayer()
        resourceUri = ""
        applicationID = ""
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        bambuserPlayer.delegate = self
        bambuserPlayer.applicationId = applicationID
        print("MonitorViewController Resource URI")
        print(resourceUri)
        if(resourceUri != "") {
            bambuserPlayer.playVideo(resourceUri)
        }
        else{
            print("No live broadcast to play")
            let ac = UIAlertController(title: "No live streams available", message: "Turn on the camera on your other device first to view.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Got it", style: .default))
            self.present(ac, animated: true)
        }
        self.view.addSubview(bambuserPlayer)
    }
    
    override func viewWillLayoutSubviews() {
        let statusBarOffset = self.topLayoutGuide.length
        bambuserPlayer.frame = CGRect(x: 0, y: 0 + statusBarOffset, width: self.view.bounds.size.width, height: self.view.bounds.size.height - statusBarOffset)
    }
    
    @objc func back(sender: UIBarButtonItem) {
        bambuserPlayer.stopVideo()
        _ = navigationController?.popViewController(animated: true)
    }
    
    func videoLoadFail() {
        NSLog("Failed to load video for %@", bambuserPlayer.resourceUri);
        let ac = UIAlertController(title: "Failure", message: "Fail to load the video", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(ac, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
