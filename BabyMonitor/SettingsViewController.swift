//
//  SettingsViewController.swift
//  BabyMonitor
//
//  Created by Dane Wikstrom on 11/26/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import RealmSwift
import Parse
import MessageUI

class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    let realm = try! Realm()
    lazy var userSettings: Results<UserSettings> = { realm.objects(UserSettings.self) }()
    var user: UserSettings!
    var currentUser = PFUser.current()
    var isRegistration = Bool()

    @IBOutlet weak var MonitorSwitch: UISwitch!
    @IBOutlet weak var CameraSwitch: UISwitch!
    @IBOutlet weak var touchIDSwitch: UISwitch!
    @IBOutlet weak var rememberSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        if userSettings.count == 1 {
            
            if user.isBroadcaster == true {
                CameraSwitch.isOn = true
                MonitorSwitch.isOn = false
            }
            else {
                CameraSwitch.isOn = false
                MonitorSwitch.isOn = true
            }
            if user.useTouchID == true {
                touchIDSwitch.isOn = true
                rememberSwitch.isOn = true
            }
            else if user.useTouchID == false {
                touchIDSwitch.isOn = false
            }
            if user.rememberMe == true {
                rememberSwitch.isOn = true
            }
            else if user.rememberMe == false {
                rememberSwitch.isOn = false
            }
        }
        else {
            
        }
    }
    
    @IBAction func SaveButtonTapped(_ sender: UIButton) {
        try! realm.write() {
            
            let updatedUserSettings = UserSettings()
            
            updatedUserSettings.username = user.username
            updatedUserSettings.passcode = user.passcode//to avoid realm migration the passcode is e mail. will do later
            
            if MonitorSwitch.isOn {
                updatedUserSettings.isMonitor = true
            } else {
                updatedUserSettings.isMonitor = false
            }
            
            if CameraSwitch.isOn {
                updatedUserSettings.isBroadcaster = true
            } else {
                updatedUserSettings.isBroadcaster = false
            }
            
            if touchIDSwitch.isOn {
                updatedUserSettings.useTouchID = true
            } else {
                updatedUserSettings.useTouchID = false
            }
            
            if rememberSwitch.isOn {
                updatedUserSettings.rememberMe = true
            } else {
                updatedUserSettings.rememberMe = false
            }
            
            realm.delete(userSettings)
            print("after delete settings")
            print(userSettings)
            
            user = updatedUserSettings
            self.realm.add(user)
            
            userSettings = realm.objects(UserSettings.self)
            print("after adding settings")
            print(userSettings)
        }
    }

    @IBAction func MonitorSwitchChanged(_ sender: UISwitch) {
        if MonitorSwitch.isOn {
            CameraSwitch.setOn(false, animated: true)
        } else {
            CameraSwitch.setOn(true, animated: true)
        }
    }
    @IBAction func CameraSwitchChanged(_ sender: UISwitch) {
        if CameraSwitch.isOn {
            MonitorSwitch.setOn(false, animated: true)
        } else {
            MonitorSwitch.setOn(true, animated: true)
        }
    }
    
    @IBAction func TouchIDSwitchChanged(_ sender: UISwitch) {
        if touchIDSwitch.isOn {
            rememberSwitch.setOn(true, animated: true)
        }
    }
    
    @IBAction func RememberMeSwitchChanged(_ sender: UISwitch) {
        if touchIDSwitch.isOn {
            rememberSwitch.setOn(true, animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Saving user settings")
        print(user)
        let vc = segue.destination as! MainViewController
        vc.user = self.user
        vc.currentUser = self.currentUser
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
