//
//  BroadcastViewController.swift
//  BabyMonitor
//
//  Created by Dane Wikstrom on 11/12/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//


import UIKit
import RealmSwift
import Parse

class BroadcastViewController: UIViewController, BambuserViewDelegate {
    let realm = try! Realm()
    lazy var userSettings: Results<UserSettings> = { realm.objects(UserSettings.self) }()
    var bambuserView : BambuserView
    var currentUser = PFUser.current()
    var broadcastButton : UIButton
    var blackoutButton : UIButton
    var blackOutButtonState = false
    let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(BroadcastViewController.back(sender:)))
    
    required init?(coder aDecoder: NSCoder) {
        bambuserView = BambuserView(preset: kSessionPresetAuto)
        bambuserView.applicationId = "MYZbycBiGZGmq4s2Hjq2tg"
        broadcastButton = UIButton(type: UIButtonType.system)
        blackoutButton = UIButton(type: UIButtonType.system)
        super.init(coder: aDecoder)
        bambuserView.delegate = self;
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        bambuserView.orientation = UIApplication.shared.statusBarOrientation
        self.view.addSubview(bambuserView.view)
        broadcastButton.addTarget(self, action: #selector(BroadcastViewController.broadcast), for: UIControlEvents.touchUpInside)
        blackoutButton.addTarget(self, action: #selector(BroadcastViewController.blackout), for: UIControlEvents.touchUpInside)
        broadcastButton.setTitle("Broadcast", for: UIControlState.normal)
        broadcastButton.setTitleColor(.red, for: UIControlState.normal)
        blackoutButton.setTitle("Blackout: OFF", for: UIControlState.normal)
        blackoutButton.setTitleColor(.red, for: UIControlState.normal)
        bambuserView.saveOnServer = false
        bambuserView.privateMode = true
        bambuserView.author = userSettings[0].username
        self.view.addSubview(broadcastButton)
        self.view.addSubview(blackoutButton)
        
        bambuserView.startBroadcasting()
        broadcast()
    }
    
    override func viewWillLayoutSubviews() {
        var statusBarOffset : CGFloat = 0.0
        statusBarOffset = CGFloat(self.topLayoutGuide.length)
        bambuserView.previewFrame = CGRect(x: 0.0, y: 0.0 + statusBarOffset, width: self.view.bounds.size.width, height: self.view.bounds.size.height - statusBarOffset)
        broadcastButton.frame = CGRect(x: 0.0, y: 10.0 + statusBarOffset, width: 100.0, height: 50.0);
        blackoutButton.frame = CGRect(x: 265.0, y: 10.0 + statusBarOffset, width: 100.0, height: 50.0);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func broadcast() {
        NSLog("Starting broadcast")
        broadcastButton.setTitle("Connecting", for: UIControlState.normal)
        broadcastButton.removeTarget(nil, action: nil, for: UIControlEvents.touchUpInside)
        broadcastButton.addTarget(bambuserView, action: #selector(bambuserView.stopBroadcasting), for: UIControlEvents.touchUpInside)
        //bambuserView.startBroadcasting()
    }
    
    func broadcastStarted() {
        NSLog("Received broadcastStarted signal")
        broadcastButton.setTitle("Stop", for: UIControlState.normal)
        broadcastButton.removeTarget(nil, action: nil, for: UIControlEvents.touchUpInside)
        broadcastButton.addTarget(bambuserView, action: #selector(bambuserView.stopBroadcasting), for: UIControlEvents.touchUpInside)
    }
    
    func broadcastStopped() {
        NSLog("Received broadcastStopped signal")
        broadcastButton.setTitle("Broadcast", for: UIControlState.normal)
        broadcastButton.removeTarget(nil, action: nil, for: UIControlEvents.touchUpInside)
        broadcastButton.addTarget(self, action: #selector(BroadcastViewController.broadcast), for: UIControlEvents.touchUpInside)
    }
    
    @objc func blackout() {
        if blackOutButtonState == false {
            blackOutButtonState = true
            navigationController?.navigationBar.barTintColor = UIColor.black
            UIBarButtonItem.appearance().tintColor = UIColor.red
            navigationItem.hidesBackButton = true
            blackoutButton.setTitle("Blackout: ON", for: UIControlState.normal)
            bambuserView.previewFrame = CGRect(x: 0.0, y: 0.0 + 0.0, width: 0.0, height: 0.0)
        }
        else {
            blackOutButtonState = false
            navigationController?.navigationBar.barTintColor = UIColor.white
            UIBarButtonItem.appearance().tintColor = UIColor.black
            navigationItem.hidesBackButton = false
            blackoutButton.setTitle("Blackout: OFF", for: UIControlState.normal)
            var statusBarOffset : CGFloat = 0.0
            statusBarOffset = CGFloat(self.topLayoutGuide.length)
            bambuserView.previewFrame = CGRect(x: 0.0, y: 0.0 + statusBarOffset, width: self.view.bounds.size.width, height: self.view.bounds.size.height - statusBarOffset)
        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        bambuserView.stopBroadcasting()
        _ = navigationController?.popViewController(animated: true)
    }
}

