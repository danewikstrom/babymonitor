////
////  CreateUserViewController.swift
////  BabyMonitor
////
////  Created by Dane Wikstrom on 11/24/17.
////  Copyright © 2017 Dane Wikstrom. All rights reserved.
////
//
//import UIKit
//import RealmSwift
//import Parse
//
//class CreateUserViewController: UIViewController, UITextFieldDelegate {
//    let realm = try! Realm()
//    lazy var userSettingsObj: Results<UserSettings> = { self.realm.objects(UserSettings.self) }()
//    var currentUser = PFUser.current()
//    @IBOutlet weak var registerButton: UIButton!
//    @IBOutlet weak var userNameTextField: UITextField!
//    @IBOutlet weak var passcodeTextField: UITextField!
//    @IBOutlet weak var isMonitorSwitch: UISwitch!
//    @IBOutlet weak var isCameraSwitch: UISwitch!
//    @IBOutlet weak var enableTouchIDSwitch: UISwitch!
//    @IBOutlet weak var rememberMeSwitch: UISwitch!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        userNameTextField.delegate = self
//        passcodeTextField.delegate = self
//
//        let ac = UIAlertController(title: "Welcome!", message: "Enter a username and 4 digit passcode. The username  and passcode will be the same for the other device that you choose to use as either a monitor or camera. Simply log with the username and passcode you set here. All settings can be changed later.", preferredStyle: .alert)
//        ac.addAction(UIAlertAction(title: "Got it", style: .default))
//        self.present(ac, animated: true)
//    }
//
//    @IBAction func RegisterButtonTapped(_ sender: UIButton) {
//
//        //validate username and passcode
//        if userNameTextField.text == "" {
//            let ac = UIAlertController(title: "No username", message: "You must enter a username", preferredStyle: .alert)
//            ac.addAction(UIAlertAction(title: "OK", style: .default))
//            self.present(ac, animated: true)
//        }
//        else if passcodeTextField.text?.count != 4 {
//            let ac = UIAlertController(title: "Invalid passcode", message: "You must enter a 4 digit passcode", preferredStyle: .alert)
//            ac.addAction(UIAlertAction(title: "OK", style: .default))
//            self.present(ac, animated: true)
//        }
//        else {
//            var signUpSuccessful = false
//            let newUser = PFUser()
//
//            newUser.username = userNameTextField.text!
//            newUser.password = passcodeTextField.text!
//
//            newUser.signUpInBackground { (succeeded, error) in
//                if error != nil {
//                    print(error ?? NSString())
//                    let ac = UIAlertController(title: "Error", message: "Registration failed.", preferredStyle: .alert)
//                    ac.addAction(UIAlertAction(title: "OK", style: .default))
//                    self.present(ac, animated: true)
//                }
//                else {
//                    self.currentUser = newUser
//                    signUpSuccessful = true
//                    print("hooray a new user was created")
//                }
//            }
//
////            if signUpSuccessful {
////                try! realm.write() {
////                    print("inside realm write")
////                    //create a userSettings object to remember the user and their settings
////                    let userSettings = UserSettings()
////
////                    userSettings.username = userNameTextField.text!
////    //                        userSettings.passcode = passcodeTextField.text!
////
////                    if isMonitorSwitch.isOn {
////                        userSettings.isMonitor = true
////                    } else {
////                        userSettings.isMonitor = false
////                    }
////
////                    if isCameraSwitch.isOn {
////                        userSettings.isBroadcaster = true
////                    } else {
////                        userSettings.isBroadcaster = false
////                    }
////
////                    if enableTouchIDSwitch.isOn {
////                        userSettings.useTouchID = true
////                    } else {
////                        userSettings.useTouchID = false
////                    }
////
////                    if rememberMeSwitch.isOn {
////                        userSettings.rememberMe = true
////                    } else {
////                        userSettings.rememberMe = false
////                    }
////
////                    //add and save the user's info and settings
////                    self.realm.add(userSettings)
////                    userSettingsObj = realm.objects(UserSettings.self)
////                    print(userSettingsObj)
////                }
////            }
//        }
//    }
//
//    @IBAction func isMonitorSwitchChanged(_ sender: UISwitch) {
//        if isMonitorSwitch.isOn {
//            isCameraSwitch.setOn(false, animated: true)
//        } else {
//            isCameraSwitch.setOn(true, animated: true)
//        }
//    }
//
//    @IBAction func isCameraSwitchChanged(_ sender: UISwitch) {
//        if isCameraSwitch.isOn {
//            isMonitorSwitch.setOn(false, animated: true)
//        } else {
//            isMonitorSwitch.setOn(true, animated: true)
//        }
//    }
//
//    @IBAction func enableTouchIDChanged(_ sender: UISwitch) {
//        if enableTouchIDSwitch.isOn {
//            rememberMeSwitch.setOn(true, animated: true)
//        }
//    }
//
//    @IBAction func rememberMeSwitchChanged(_ sender: UISwitch) {
//        if enableTouchIDSwitch.isOn {
//            rememberMeSwitch.setOn(true, animated: true)
//        }
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool
//    {
//        // Hide the keyboard.
//        textField.resignFirstResponder()
//        return true
//    }
//
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let vc = segue.destination as! Login
//        vc.currentUser = currentUser
//        vc.user = userSettingsObj[0]
//    }
//
//
//}
//
