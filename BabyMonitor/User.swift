//
//  User.swift
//  BabyMonitor
//
//  Created by Dane Wikstrom on 11/24/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import Foundation
import RealmSwift

class UserSettings : Object {
    @objc dynamic var username = ""
    @objc dynamic var passcode = ""//do avoid realm migration this is email. will do later
    @objc dynamic var useTouchID = Bool();
    @objc dynamic var isMonitor = Bool();
    @objc dynamic var isBroadcaster = Bool();
    @objc dynamic var rememberMe = Bool();
}

