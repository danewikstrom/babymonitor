//
//  MainViewController.swift
//
//
//  Created by Dane Wikstrom on 11/24/17.
//

import UIKit
import RealmSwift
import Parse

class MainViewController: UIViewController {
    let realm = try! Realm()
    lazy var userSettings: Results<UserSettings> = { realm.objects(UserSettings.self) }()
    var user: UserSettings!
    var currentUser = PFUser.current()
    var resourceUri = ""
    let apiKey = "4995px51rwjqkvfkdy28hhlt5"
    let applicationID = "MYZbycBiGZGmq4s2Hjq2tg"
    var isRegistration = Bool()
    var loggingOut = Bool()
    @IBOutlet weak var startCameraButton: UIButton!
    @IBOutlet weak var monitorBabyButton: UIButton!
    @IBOutlet weak var setupButton: UIButton!
    @IBOutlet weak var webcamIcon: UIImageView!
    @IBOutlet weak var sleepingBabyIcon: UIImageView!
    @IBOutlet weak var refreshButton: UIButton!
    
    var goingToSettings = false
    var hasBroadcast = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Main - user")
        print(user)
        // Do any additional setup after loading the view.
        
        //if the user has set up their settings
        if isRegistration == false && user != nil{
            setupButton.isEnabled = false
            setupButton.isHidden = true
            
            if user.isMonitor {
                startCameraButton.isEnabled = false
                startCameraButton.isHidden = true
                webcamIcon.isHidden = true
                sleepingBabyIcon.isHidden = false
                refreshButton.isHidden = false
                refreshButton.isEnabled = true
                
                LoadLiveBroadcast()
            }
            else {
                monitorBabyButton.isEnabled = false
                monitorBabyButton.isHidden = true
                webcamIcon.isHidden = false
                sleepingBabyIcon.isHidden = true
                refreshButton.isHidden = true
                refreshButton.isEnabled = false
            }
        }

        else { //display the set up button
            setupButton.isEnabled = true
            setupButton.isHidden = false
            
            monitorBabyButton.isEnabled = false
            monitorBabyButton.isHidden = true
            
            startCameraButton.isEnabled = false
            startCameraButton.isHidden = true
            
            webcamIcon.isHidden = true
            
            refreshButton.isHidden = true
            refreshButton.isEnabled = false
            
            let ac = UIAlertController(title: "Setting up second device", message: "When you are done setting up this device, login into your second device with the same info you registered with and go to settings to configure once logged in.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
    
    func LoadLiveBroadcast() {
        let url = URL(string: "https://api.irisplatform.io/broadcasts")
        var request = URLRequest(url: url!)
        
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(apiKey)", forHTTPHeaderField: "Authorization")
        request.addValue("application/vnd.bambuser.v1+json", forHTTPHeaderField: "Accept")
        request.addValue("\(user.username)", forHTTPHeaderField: "byAuthors")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            let dictionary = json
            let results = dictionary!["results"] as? [[String: Any]]
            var result = results?.first
            if result == nil {
                print("There is no resourceURI available to view a live broadcast")
            }
            else {
                self.hasBroadcast = true
                self.resourceUri = result?["resourceUri"] as! String
            }
            
            print(json!)
            print("RESOURCE URI")
            print(self.resourceUri)
        }
        task.resume()
        
        if hasBroadcast {
            monitorBabyButton.isEnabled = true
            monitorBabyButton.isHidden = false
        }
    }

    @IBAction func SetupButtonTapped(_ sender: UIButton) {
        goingToSettings = true
        performSegue(withIdentifier: "ShowSettings", sender: self)
    }
    
    @IBAction func MonitorBabyButtonTapped(_ sender: UIButton) {
        if hasBroadcast {
            //performSegue(withIdentifier: "ShowMonitorViewController", sender: self)
        }
        else {
            let ac = UIAlertController(title: "Start broadcast first", message: "You must start a broadcast on the device you have choosen to use as a camera before viewing. Once you have started broadcasting from your other device tap the Refresh button.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
    
    @IBAction func RefreshButtonTapped(_ sender: UIButton) {
        LoadLiveBroadcast()
    }
    
    @IBAction func SettingsButtonTapped(_ sender: UIButton) {
        goingToSettings = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if goingToSettings {
            let svc = segue.destination as! SettingsViewController
            svc.user = self.user
            svc.currentUser = self.currentUser
            svc.isRegistration = self.isRegistration
        }
            
        else if user.isMonitor && loggingOut == false {
            let vc = segue.destination as! MonitorViewController
            vc.resourceUri = self.resourceUri
            vc.applicationID = self.applicationID
            vc.currentUser = self.currentUser
        }
    }

    @IBAction func LogoutButtonTapped(_ sender: Any) {
        loggingOut = true
        PFUser.logOutInBackground { (error) in
            if(error != nil) {
                print(error ?? NSString())
                let ac = UIAlertController(title: "Logout Failed", message: error as? String, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(ac, animated: true)
            }
            print(PFUser.current()?["username"] as? String! as Any )
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        goingToSettings = false
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if isRegistration == false && user != nil {
            setupButton.isEnabled = false
            setupButton.isHidden = true
            
            if user.isMonitor {
                startCameraButton.isEnabled = false
                startCameraButton.isHidden = true
                
                LoadLiveBroadcast()
            }
            else {
                monitorBabyButton.isEnabled = false
                monitorBabyButton.isHidden = true
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

