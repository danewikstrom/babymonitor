//
//  ViewController.swift
//  BabyMonitor
//
//  Created by Dane Wikstrom on 11/12/17.
//  Copyright © 2017 Dane Wikstrom. All rights reserved.
//

import UIKit
import RealmSwift
import Parse
import LocalAuthentication

class Login: UIViewController, UITextFieldDelegate {
    lazy var userSettings: Results<UserSettings> = { self.realm.objects(UserSettings.self) }()
    let realm = try! Realm()
    var user: UserSettings!
    var currentUser = PFUser.current()
    var isRegistration = Bool()
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passcodeTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        userNameTextField.delegate = self
        passcodeTextField.delegate = self
        emailTextField.delegate = self
//
//        try! realm.write
//        {
//            realm.delete(userSettings)
//        }

        print(userSettings)
        print(userSettings.count)

        
        //if the user has set their settings
        if(userSettings.count == 1) {
            user = userSettings[0]
            
            //if touchID is enabled then set the username textfield to the user's username and then authenticate user using touch id
            //a user must have a valid session token to use touch id (already be logged in)
            if user.useTouchID == true {
                userNameTextField.text = user.username
                emailTextField.text = user.passcode //to avoid realm migration the passcode is e mail. will do later
                AuthenticateUser()
            }
                //set the username textfield to the user's username
            else if user.rememberMe == true {
                userNameTextField.text = user.username
                emailTextField.text = user.passcode //to avoid realm migration the passcode is e mail. will do later
            }
        }
    }
    @IBAction func RegisterButtonTapped(_ sender: UIButton) {
        if userNameTextField.text == "" {
            let ac = UIAlertController(title: "No username", message: "You must enter a username", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
        if emailTextField.text == "" {
            let ac = UIAlertController(title: "No email", message: "You must enter an email address", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
        else if passcodeTextField.text?.count != 4 {
            let ac = UIAlertController(title: "Invalid passcode", message: "You must enter a 4 digit passcode", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
        else {
            
            var registerSuccesfull = false
            let newUser = PFUser()
            
            newUser.username = userNameTextField.text!
            newUser.password = passcodeTextField.text!
            newUser.email = emailTextField.text!
            
            let username = userNameTextField.text!
            let email = emailTextField.text!
            
            newUser.signUpInBackground { (succeeded, error) in
                if error != nil {
                    print(error ?? NSString())
                    let ac = UIAlertController(title: "Error", message: "Registration failed.", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                }
                else {
                    registerSuccesfull = true
                    self.isRegistration = true
                    self.currentUser = newUser
                    print("hooray a new user was created")
                    self.currentUser = newUser
                    if registerSuccesfull {
                        try! self.realm.write() {
                            let newUserSettings = UserSettings()
                            
                            newUserSettings.username = username
                            newUserSettings.passcode = email
                            newUserSettings.isMonitor = true
                            newUserSettings.isBroadcaster = false
                            newUserSettings.rememberMe = false
                            newUserSettings.useTouchID = false
                            
                            self.realm.add(newUserSettings)
                            self.userSettings = self.realm.objects(UserSettings.self)
                            self.user = newUserSettings
                            print("Register - user")
                            print(self.user)
                        }
                    }
                    self.NavigateToMainViewController()
                }
                
            }
            
        }
    }
    
    @IBAction func LoginButtonTapped(_ sender: UIButton) {
        print(userNameTextField.text!)
        print(passcodeTextField.text!)
        print(emailTextField.text!)
        
        let newUser = PFUser()
        let username = userNameTextField.text!
        let email = emailTextField.text!
        
        if user == nil {
            newUser.username = userNameTextField.text!
            newUser.password = passcodeTextField.text!
            newUser.email = emailTextField.text!
        }
        PFUser.logInWithUsername(inBackground: userNameTextField.text!, password: passcodeTextField.text!, block: { (user, error) in
            if(self.userSettings.count == 0){
                    print(user!["username"])
                }
                if(error == nil) {
                    if self.userSettings.count == 0 {
                        self.currentUser = newUser
                        try! self.realm.write() {
                            let newUserSettings = UserSettings()
                            
                            newUserSettings.username = username
                            newUserSettings.passcode = email
                            newUserSettings.isMonitor = true
                            newUserSettings.isBroadcaster = false
                            newUserSettings.rememberMe = false
                            newUserSettings.useTouchID = false
                            
                            self.realm.add(newUserSettings)
                            self.userSettings = self.realm.objects(UserSettings.self)
                            self.user = newUserSettings
                            print("New user - user")
                            print(self.user)
                        }
                    }
                    self.NavigateToMainViewController()
                }
                if(error != nil) {
                    print(error ?? NSString())
                    let ac = UIAlertController(title: "Login Failed", message: "Incorrect Username or Passcode", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(ac, animated: true)
                }
            })
            if (PFUser.current() != nil) {
                print(PFUser.current()?["username"] as? String! as Any )
            }
    }
    

    @IBAction func TouchIDButtonTapped(_ sender: UIButton) {
        AuthenticateUser()
    }
    
    func AuthenticateUser() {
        let context = LAContext()
        var error: NSError?
        if userSettings.count == 0 {
            let ac = UIAlertController(title: "Register First", message: "Register first. You can setup Touch ID authentication in settings.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
        else {
            if user.username == userNameTextField.text {
                if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                    let reason = "Touch ID Login"
                    
                    context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                        [unowned self] success, authenticationError in
                        
                        DispatchQueue.main.async {
                            if success {
                                self.NavigateToMainViewController()
                            }
                        }
                    }
                }
                else {
                    let ac = UIAlertController(title: "Touch ID not available", message: "Your device is not configured for Touch ID.", preferredStyle: .alert)
                    ac.addAction(UIAlertAction(title: "OK", style: .default))
                    present(ac, animated: true)
                }
            }
        }
    }
    
    func NavigateToMainViewController() {
        performSegue(withIdentifier: "ShowMainViewController", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
//    //https://teamtreehouse.com/community/parse-swift-email-password-reset
//    @IBAction func ForgotPasswordButtonTapped(_ sender: UIButton) {
//        let titlePrompt = UIAlertController(title: "Reset password",
//                                            message: "Enter the email you registered with:",
//                                            preferredStyle: .alert)
//
//        var titleTextField: UITextField?
//        titlePrompt.addTextField { (textField) -> Void in
//            titleTextField = textField
//            textField.placeholder = "Email"
//        }
//
//        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
//
//        titlePrompt.addAction(cancelAction)
//
//        titlePrompt.addAction(UIAlertAction(title: "Reset", style: .destructive, handler: { (action) -> Void in
//            if let textField = titleTextField {
//                self.resetPassword(email: textField.text!)
//            }
//        }))
//
//        self.present(titlePrompt, animated: true, completion: nil)
//    }
//
//    func resetPassword(email : String){
//
//        // convert the email string to lower case
//        let emailToLowerCase = email.lowercased()
//        // remove any whitespaces before and after the email address
//        let emailClean = emailToLowerCase.trimmingCharacters(in: .whitespaces)
//        print("emailClean")
//        print(emailClean)
//        PFUser.requestPasswordResetForEmail(inBackground: emailClean) { (success, error) -> Void in
//            if (error == nil) {
//                let success = UIAlertController(title: "Success", message: "Success! Check your email!", preferredStyle: .alert)
//                let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
//                success.addAction(okButton)
//                self.present(success, animated: false, completion: nil)
//
//            }else {
//                let error = UIAlertController(title: "Cannot complete request", message: error as? String, preferredStyle: .alert)
//                let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
//                error.addAction(okButton)
//                self.present(error, animated: false, completion: nil)
//            }
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nav = segue.destination as! UINavigationController
        let mvc = nav.topViewController as! MainViewController
        print("VC user in prepare")
        mvc.user = user
        mvc.currentUser = self.currentUser
        mvc.isRegistration = self.isRegistration
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

